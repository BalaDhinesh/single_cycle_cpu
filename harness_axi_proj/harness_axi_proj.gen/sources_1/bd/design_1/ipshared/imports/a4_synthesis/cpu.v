module cpu (
    input clk,
    input reset,
    output reg [31:0] iaddr,
    input [31:0] idata,
    output [31:0] daddr,
    input [31:0] drdata,
    output [31:0] dwdata,
    output [3:0] dwe,
    output [32*32-1:0] registers
);


/************************************************** 
                    FETCH-PC
**************************************************/
  wire [31:0] next_pc;


/************************************************** 
                    DECODE
**************************************************/
  wire [31:0] imm;
  wire [4:0] rs1, rs2, rd;
  wire [6:0] opcode;
  wire w_en, is_immediate;
  wire is_load, is_lui, is_jump;


/************************************************** 
                REGISTER-MEMORY
**************************************************/
  wire [31:0] reg_write;
  wire [31:0] rv1;
  wire [31:0] rv2;


/************************************************** 
                  ALU-EXECUTE
**************************************************/
  wire [3:0] ALUop;
  wire br_consider;
  wire [31:0] alu_out;


/************************************************** 
                  DATA-MEMORY
**************************************************/
  wire [31:0] ldata;


  // Program Counter Module
  FETCH PC_Count(
    .PC(iaddr),
    .imm(imm),
    .idata(idata),
    .br_consider(br_consider),
    .alu_out(alu_out),
    .next_pc(next_pc)
  );

  // Decode and control logic
  DECODE decode(
    .idata(idata),
    .rs1(rs1),
    .rs2(rs2),
    .rd(rd),
    .opcode(opcode),
    .imm(imm),
    .w_en(w_en)
  );
  
  assign is_load = (idata[6:0] == 7'b0000011);
  assign is_lui = (opcode == 7'b0110111);
  assign is_jump = (opcode == 7'b1101111 || opcode == 7'b1100111);
  assign reg_write = is_lui ? imm :              // Immediate data for LUI
                     is_jump ? iaddr + 4 :       // PC + 4 for jump
                     is_load ? ldata  :          // Data from memory for load
                     alu_out ;                   // Normal ALU operations
  assign ALUop = (opcode == 7'b0010111) ? 4'b0000 : 
                 (opcode == 7'b0010011) ? {1'b0, idata[14:12]} : 
                 {idata[30], idata[14:12]};       // Encoding for ALU operation

  assign is_immediate = (opcode == 7'b0010011 || opcode == 7'b0000011 || 
                         opcode == 7'b1100111 || opcode == 7'b0010111 );  // Immediate for rv2

  REGISTER register(
    .rs1(rs1),
    .rs2(rs2),
    .rd(rd),
    .w_en(w_en),
    .reset(reset),
    .wdata(reg_write),
    .rv1(rv1),
    .rv2(rv2),
    .clk(clk),
    .registers(registers)
  );



  ALU alu32(
    .op(ALUop),
    .rv1((opcode == 7'b0010111) ? iaddr : rv1),        // AUIPC , Other
    .rv2( is_immediate ? imm : rv2),                  // Load Immediate / From Register 2
    .is_branch(idata[6:2] == 5'b11000),
    .br_consider(br_consider),
    .alu_out(alu_out)
    );

  DATA_MEMORY memory(
      .idata(idata),
      .drdata(drdata),
      .rv1(rv1),
      .rv2(rv2),
      .imm(imm),
      .ldata(ldata),
      .daddr(daddr),
      .dwdata(dwdata),
      .dwe(dwe)
  );

  always @(posedge clk) begin
      if (!reset) begin
        iaddr <= 32'h00000000;
      end else begin
        iaddr <= next_pc;
      end
    end

endmodule
