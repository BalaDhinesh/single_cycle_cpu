module ALU(
    input [3:0] op,             // ALU input encoding
    input [31:0] rv1,           // First operand
    input [31:0] rv2,           // Second operand
    input is_branch,            // ALU for branch
    output reg br_consider,     // check for valid branch
    output reg [31:0] alu_out     // Output value
);
    always @(*) begin
      if(is_branch) begin
        alu_out = 0;
        // check for branch valid
        case(op[2:0])
          3'b000: br_consider = (rv1 == rv2);                     // BEQ                         
          3'b001: br_consider = !(rv1 == rv2);                    // BNE                         
          3'b100: br_consider = ($signed(rv1) < $signed(rv2)) ;   // BLT                                                                     
          3'b101: br_consider = ($signed(rv1) >= $signed(rv2));   // BGE
          3'b110: br_consider = (rv1 < rv2) ;                     // BLTU                                                   
          3'b111: br_consider = (rv1 >= rv2);                     // BGEU
          default: br_consider = 0;                    
        endcase
      end
      else begin
        br_consider = 0;
        case(op[2:0])                                                                                       
        3'b000: alu_out = op[3] ?  rv1 - rv2 : rv1 + rv2;                   // SUB/ADD                                 
        3'b001: alu_out = $signed(rv1) << $signed(rv2[4:0]);                // SLL                               
        3'b010: alu_out = ($signed(rv2) > $signed(rv1)) ? 32'h1 : 32'h0;        // SLT                                 
        3'b011: alu_out = (rv2 > rv1) ? 32'h1 : 32'h0;                            // SLTU                             
        3'b100: alu_out = rv1 ^ rv2;                                        // XOR                             
        3'b101: alu_out = (op[3]) ? $signed(rv1) >>> $signed(rv2[4:0]) :    // SRA / SRL
                           $signed(rv1) >> $signed(rv2[4:0]);  
        3'b110: alu_out = rv1 | rv2;                                        // OR 
        3'b111: alu_out = rv1 & rv2;                                        // AND
        default: alu_out = 0;
      endcase
      end
    end

endmodule
