# single_cycle_cpu

All files are present inside _files_ directory. Vivado project file is present inside _harness_axi_proj_ directory. To run the design on PYNQ, simply execute all snippets in the _cpu.ipynb_ file.

## Module hierarchy:
![module-hierarchy](https://i.imgur.com/RSGEkXi.png)