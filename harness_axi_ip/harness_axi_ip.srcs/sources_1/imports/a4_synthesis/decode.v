module DECODE(
    input [31:0] idata,
    output [4:0] rs1,
    output [4:0] rs2,
    output [4:0] rd,
    output [6:0] opcode,
    output [31:0] imm,
    output w_en
);

    assign rs1 = idata[19:15];              // Source register 1
    assign rs2 = idata[24:20];              // Source register 2
    assign rd = idata[11:7];                // Destination register 
    assign opcode = idata[6:0];             // opcode

    // Immediate generation
    assign imm = (opcode == 7'b0100011) ? {{20{idata[31]}},idata[31:25],idata[11:7]} :                  // S-type immediate
                 (opcode == 7'b0110111 || opcode == 7'b0010111) ? {idata[31:12],{12{1'b0}}} :           // U-type immediate
                 (opcode == 7'b1101111) ? {{12{idata[31]}},idata[19:12],idata[20],idata[30:21], 1'b0}:  // J-type immediate
                 (opcode == 7'b1100011) ? {{20{idata[31]}},idata[7],idata[30:25],idata[11:8],1'b0} :    // B-type immediate
                 {{20{idata[31]}},idata[31:20]};                                                        // I-type immediate
        
    // Register write enable
    assign w_en = (opcode == 7'b0000011 || opcode == 7'b0010011 || 
                   opcode == 7'b0110011 || opcode == 7'b1100111 || 
                   opcode == 7'b1101111 || opcode == 7'b0110111 || 
                   opcode == 7'b0010111
                   );

endmodule