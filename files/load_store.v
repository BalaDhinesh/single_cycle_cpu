module DATA_MEMORY(
    input [31:0] idata,
    input [31:0] drdata,
    input [31:0] rv1,
    input [31:0] rv2,
    input [31:0] imm,
    output [31:0] ldata,
    output reg [31:0] daddr,
    output reg [31:0] dwdata,
    output reg [3:0] dwe
);
    wire is_store;
    wire is_load;

    assign is_store = (idata[6:0] == 7'b0100011);
    assign is_load = idata[6:0] == 7'b0000011;

    // Alignment handle for load
    assign ldata =  (idata[14:12] == 3'b000) || (idata[14:12] == 3'b100) ? ( 32'h000000ff & (drdata << 6) >> 6) : 
                    (idata[14:12] == 3'b001) || (idata[14:12] == 3'b101) ? ( 32'h00000fff &(drdata << 4) >> 4) : 
                    drdata;
   
    always @(*) begin
        daddr = rv1 + imm; 
        dwdata = (idata[14:12] == 3'b000) ? {4{rv2[7:0]}} :
                (idata[14:12] == 3'b001) ? {2{rv2[15:0]}}:
                rv2;

        // Alignment handle for Store
        case({idata[14:12], is_store})
            4'b0001: begin
                if(daddr[1:0] == 2'b00) dwe = 4'b0001;
                else if(daddr[1:0] == 2'b01) dwe = 4'b0010;
                else if(daddr[1:0] == 2'b10) dwe = 4'b0100;
                else dwe = 4'b1000;
            end
            4'b0011: begin
                if(daddr[1:0] == 2'b00) dwe = 4'b0011;
                else if(daddr[1:0] == 2'b10) dwe = 4'b1100;
                else dwe = 4'b0000;
            end
            4'b0101: begin
                if(daddr[1:0] == 2'b00) dwe = 4'b1111;
                else dwe = 4'b0000;
            end
            default: dwe = 4'b0000;
        endcase
    end
endmodule