module FETCH(
    input [6:0] PC,
    input [31:0] imm,
    input br_consider,
    input [31:0] idata,
    input [31:0] alu_out,
    output [6:0] next_pc
    );

    wire [6:0] PC_branch;
    wire is_branch, is_jal, is_jalr;
    
    assign PC_branch = PC + imm;
    assign is_branch = idata[6:2] == 5'b11000;
    assign is_jal = idata[6:2] == 5'b11011;
    assign is_jalr = idata[6:2] == 5'b11001;

    assign next_pc = ((is_branch & br_consider) || is_jal) ? PC_branch : 
                        is_jalr ? (alu_out & 32'hFFFFFFFE) : 
                        PC + 4;

endmodule
