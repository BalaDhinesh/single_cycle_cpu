module REGISTER(
    input clk,
    input [4:0] rs1,    
    input [4:0] rs2,     
    input [4:0] rd,      
    input w_en,      
    input reset,
    input [31:0] wdata,  
    output [31:0] rv1,   
    output [31:0] rv2,
    output reg [32*32-1:0] registers
);
    integer i;
	reg [32-1:0] reg31, reg30, reg29, reg28, reg27, reg26, reg25, reg24, reg23, reg22, reg21, reg20, reg19, reg18, reg17, reg16, reg15, reg14, reg13, reg12, reg11, reg10, reg9, reg8, reg7, reg6, reg5, reg4, reg3, reg2, reg1, reg0;
//    assign {reg31, reg30, reg29, reg28, reg27, reg26, reg2, reg24, reg23, reg22, reg21, reg20, reg19, reg18, reg17, 
//           reg16, reg15, reg14, reg13, reg12, reg11, reg10, reg9, reg8, reg7, reg6, reg5, reg4, reg3, reg2, reg1, reg0} = registers;
//    reg [31:0] reg_mem[31:0];
    initial begin
        {reg31, reg30, reg29, reg28, reg27, reg26, reg25, reg24, reg23, reg22, reg21, reg20, reg19, reg18, reg17, 
           reg16, reg15, reg14, reg13, reg12, reg11, reg10, reg9, reg8, reg7, reg6, reg5, reg4, reg3, reg2, reg1, reg0} = 0;
//      for  (i=0; i<32; i=i+1) reg_mem[i] = 0;
    end

    always @(posedge clk or posedge reset) begin
      if(reset) begin
        {reg31, reg30, reg29, reg28, reg27, reg26, reg25, reg24, reg23, reg22, reg21, reg20, reg19, reg18, reg17, 
           reg16, reg15, reg14, reg13, reg12, reg11, reg10, reg9, reg8, reg7, reg6, reg5, reg4, reg3, reg2, reg1, reg0} = 0;
      end
      else begin
        if((w_en)&(rd!= 0)) begin
            case ( rd )
	        5'd0 : reg0 = wdata;
            5'd1 : reg1 = wdata;
            5'd2 : reg2 = wdata;
            5'd3 : reg3 = wdata;
            5'd4 : reg4 = wdata;
            5'd5 : reg5 = wdata;
            5'd6 : reg6 = wdata;
            5'd7 : reg7 = wdata;
            5'd8 : reg8 = wdata;
            5'd9 : reg9 = wdata;
            5'd10 : reg10 = wdata;
            5'd11 : reg11 = wdata;
            5'd12 : reg12 = wdata;
            5'd13 : reg13 = wdata;
            5'd14 : reg14 = wdata;
            5'd15 : reg15 = wdata;
            5'd16 : reg16 = wdata;
            5'd17 : reg17 = wdata;
            5'd18 : reg18 = wdata;
            5'd19 : reg19 = wdata;
            5'd20 : reg20 = wdata;
            5'd21 : reg21 = wdata;
            5'd22 : reg22 = wdata;
            5'd23 : reg23 = wdata;
            5'd24 : reg24 = wdata;
            5'd25 : reg25 = wdata;
            5'd26 : reg26 = wdata;
            5'd27 : reg27 = wdata;
            5'd28 : reg28 = wdata;
            5'd29 : reg29 = wdata;
            5'd30 : reg30 = wdata;
            5'd31 : reg31 = wdata;
	      endcase
	    end
//        registers <= {registers[32*32-1:32*rd], wdata, registers[32*(rd-1):0]};
//        registers[32*rd+:32] <= wdata;
        else reg0 = 0;
      end
    end
    always@(*)
     registers = {reg31, reg30, reg29, reg28, reg27, reg26, reg25, reg24, reg23, reg22, reg21, reg20, reg19, reg18, reg17, 
                   reg16, reg15, reg14, reg13, reg12, reg11, reg10, reg9, reg8, reg7, reg6, reg5, reg4, reg3, reg2, reg1, reg0};
    assign rv1 = registers[32*rs1+:32];
    assign rv2 = registers[32*rs2+:32];

endmodule